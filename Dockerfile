FROM openjdk:8-jdk-alpine
MAINTAINER Gabriel Ribeiro

ENV RUN_USER            bamboo
ENV RUN_GROUP           bamboo

# https://confluence.atlassian.com/display/BAMBOO/Locating+important+directories+and+files
ENV BAMBOO_AGENT_HOME          /var/atlassian/application-data/bamboo_agent_home
ENV GRADLE_HOME /usr/lib/gradle
ENV PATH ${PATH}:${GRADLE_HOME}/bin

VOLUME ["${BAMBOO_AGENT_HOME}"]

WORKDIR $BAMBOO_AGENT_HOME


#ENTRYPOINT ["tail", "-f", "/dev/null"]

# Gradle version to install
ARG GRADLE_INSTALL_VERSION=2.9
ARG GRADLE_DOWNLOAD_URL=https://services.gradle.org/distributions/gradle-${GRADLE_INSTALL_VERSION}-bin.zip

# GIT LFS version to install
ARG GIT_LFS_VERSION=2.2.1
# GIT LFS URL
ARG GIT_LFS_DOWNLOAD_URL=https://github.com/github/git-lfs/releases/download/v${GIT_LFS_VERSION}/git-lfs-linux-amd64-${GIT_LFS_VERSION}.tar.gz

# Install dependencies and server capabilities
RUN apk update -qq \
    && apk add wget curl git maven apache-ant unzip openssh bash procps openssl perl ttf-dejavu tini \
    && rm -rf /var/lib/{apt,dpkg,cache,log}/ /tmp/* /var/tmp/* \
    && curl -L --silent ${GIT_LFS_DOWNLOAD_URL} | tar -xz --strip-components=1 -C "/usr/bin" git-lfs-${GIT_LFS_VERSION}/git-lfs
# Install glibc as per https://community.atlassian.com/t5/Bamboo-questions/Bamboo-remote-agent-5-13-0-1-not-working-inside-alpine-3-4/qaq-p/353526
RUN wget -q -O /etc/apk/keys/sgerrand.rsa.pub https://raw.githubusercontent.com/sgerrand/alpine-pkg-glibc/master/sgerrand.rsa.pub \
    && wget https://github.com/sgerrand/alpine-pkg-glibc/releases/download/2.25-r0/glibc-2.25-r0.apk \
    && apk add glibc-2.25-r0.apk \
    && rm glibc-2.25-r0.apk
 # Install gradle
RUN cd /tmp \
	&& wget ${GRADLE_DOWNLOAD_URL} \
	&& unzip gradle-${GRADLE_INSTALL_VERSION}-bin.zip -d /opt \
	&& rm gradle-${GRADLE_INSTALL_VERSION}-bin.zip \
	&& ln -s /opt/gradle-${GRADLE_INSTALL_VERSION} /usr/lib/gradle
# Create Bamboo user and Agent home folder and set the proper ownership 
RUN addgroup -S ${RUN_GROUP} \
    && adduser -S -G ${RUN_GROUP} ${RUN_USER} \
    && mkdir -p ${BAMBOO_AGENT_HOME} \
    && chown -R ${RUN_GROUP}:${RUN_USER} ${BAMBOO_AGENT_HOME} \
    && chmod -R 755 "${BAMBOO_AGENT_HOME}"


#USER ${RUN_USER}
COPY entrypoint.sh /
ENTRYPOINT ["/entrypoint.sh"]




