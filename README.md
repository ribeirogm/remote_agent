This is Docker image of a remote agent for Atlassian Bamboo


## Usage

Create Docker container and pass URL to your Bamboo installation
using `BAMBOO_SERVER` environment variable:

```bash
docker run -d --name="bamboo-agent" -e BAMBOO_SERVER=http://bambooURL:8085 ribeirogm/bamboo-agent
```

You can use the `JAVA_OPTS` variable to pass java arguments:

```bash
docker run -d --name="bamboo-agent" -e BAMBOO_SERVER=http://bambooURL:8085 -e JAVA_OPTS=-Dbamboo.agent.ignoreServerCertName=true ribeirogm/bamboo-agent
```
If "Security token verification" is enabled, you can use the `TOKEN` variable to pass the value
See https://confluence.atlassian.com/bamboo/agent-authentication-289277196.html
for more information.

```bash
docker run -d --name="bamboo-agent" -e BAMBOO_SERVER=http://bambooURL:8085 -e JAVA_OPTS=-Dbamboo.agent.ignoreServerCertName=true -e TOKEN=f3defca0b1dee4b665fa7471cfe2899d524722f9 ribeirogm/bamboo-agent
```

If the "Remote Agent Authentication" option is enabled, you will need to approve access for a new remote agent in Bamboo
administration console.
See https://confluence.atlassian.com/display/BAMBOO/Agent+authentication
for more information.

## Internals

On first run agent JAR is downloaded from URL to `bamboo-agent-home`folder:
```bash
${BAMBOO_SERVER}/agentServer/agentInstaller/atlassian-bamboo-agent-installer.jar
```
The `bamboo-agent-home`folder is located inside `/var/atlassian/application-data/bamboo_agent_home`
