#!/bin/bash
set -euo pipefail

: ${BAMBOO_SERVER:=}
: ${JAVA_OPTS:=}
: ${TOKEN:=}

cd ${BAMBOO_AGENT_HOME} 

JAVA_OPTS="${JAVA_OPTS} -Dbamboo.home=${BAMBOO_AGENT_HOME}"
AGENT_JAR=atlassian-bamboo-agent-installer.jar


if [ -n "${TOKEN}" ]; then
    AGENT_OPTS="${BAMBOO_SERVER} -t ${TOKEN} $@"
else
    AGENT_OPTS="${BAMBOO_SERVER} $@"
fi

if [ -z "${BAMBOO_SERVER}" ]; then
	echo "Bamboo server URL undefined!" >&2
	echo "Please set BAMBOO_SERVER environment variable to URL of your Bamboo instance." >&2
	exit 1
fi

if [ ! -f ${AGENT_JAR} ]; then
	echo "Downloading agent JAR..."
	wget "-O${AGENT_JAR}" "${BAMBOO_SERVER}/agentServer/agentInstaller/${AGENT_JAR}"
fi

AGENT_RUN_COMMAND="java ${JAVA_OPTS} -jar ${AGENT_JAR} ${AGENT_OPTS}"

# Start Bamboo as the correct user
if [ "${UID}" -eq 0 ]; then
    echo "User is currently root. Will change directories to daemon control, then downgrade permission to daemon"
    chmod -R 700 "${BAMBOO_AGENT_HOME}" &&
        chown -R "${RUN_USER}:${RUN_GROUP}" "${BAMBOO_AGENT_HOME}"
    # Now drop privileges
    exec su -s /bin/bash "${RUN_USER}" -c "${AGENT_RUN_COMMAND}"
else
    exec "${AGENT_RUN_COMMAND}"
fi